NAME = fdf
FILES = main.c get_next_line.c split.c bresenham.c key_func.c key_func2.c mix.c
OBJECTS = main.o get_next_line.o split.o bresenham.o key_func.o key_func2.o mix.o
FLAGS = -framework OpenGL -framework AppKit -Wall -Wextra -Werror

all: $(NAME)

$(NAME):
	@gcc $(FLAGS) $(FILES) -I ./libft/includes/ -I ./minilibx_macos/ -L ./libft/ -lft -L ./minilibx_macos/ -lmlx -o $(NAME)
	@echo "Все файлы собраны!"

$(OBJECTS):
	@gcc $(FLAGS) -c $(NAME) $(FILES) -I ./libft/includes/ -I ./minilibx_macos/ -L ./libft/ -lft -L ./minilibx_macos/ -lmlx
	@make -C ./libft/
	@make -C ./minilibx_macos/
	@echo "Объектные файлы собраны!"

clean:
	@rm -f $(OBJECTS)
	@make clean -C ./libft/
	@make clean -C ./minilibx_macos/
	@echo "Объектные файлы уничтожены!"

fclean: clean
	@rm -f $(NAME)
	@echo "Исполняемые файлы уничтожены!"

re: fclean all
