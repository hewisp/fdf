/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bresenham.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/03 18:01:48 by hewisp            #+#    #+#             */
/*   Updated: 2019/05/03 18:02:07 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	ft_swap_double(double *a, double *b)
{
	double pointer;

	pointer = *a;
	*a = *b;
	*b = pointer;
}

char	bresenham2(double *x0, double *y0, double *x1, double *y1)
{
	char steep;

	steep = 0;
	if (fabs((*y1) - (*y0)) > fabs((*x1) - (*x0)))
		steep = 1;
	if (steep)
	{
		ft_swap_double(x0, y0);
		ft_swap_double(x1, y1);
	}
	if ((*x0) > (*x1))
	{
		ft_swap_double(x0, x1);
		ft_swap_double(y0, y1);
	}
	return (steep);
}

void	bresenham3(double *dx)
{
	dx[0] = dx[8] - dx[6];
	dx[1] = fabs(dx[9] - dx[7]);
	dx[2] = dx[0] / 2;
	dx[3] = (dx[7] < dx[9]) ? 1 : -1;
	dx[4] = dx[7];
	dx[5] = dx[6];
}

void	ft_bresenham(t_point first, t_point second, t_map **map)
{
	double	dx[10];
	char	steep;

	dx[6] = first.x * (*map)->zoom;
	dx[7] = first.y * (*map)->zoom;
	dx[8] = second.x * (*map)->zoom;
	dx[9] = second.y * (*map)->zoom;
	steep = bresenham2(&dx[6], &dx[7], &dx[8], &dx[9]);
	bresenham3(dx);
	while (dx[5] <= dx[8])
	{
		mlx_pixel_put((*map)->mlx_ptr, (*map)->mlx_window
		, (steep ? dx[4] : dx[5]) + (*map)->start_x
		, (steep ? dx[5] : dx[4]) + (*map)->start_y, (*map)->color);
		dx[2] -= dx[1];
		if (dx[2] < 0)
		{
			dx[4] += dx[3];
			dx[2] += dx[0];
		}
		dx[5]++;
	}
}
