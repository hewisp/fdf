/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 17:40:38 by hewisp            #+#    #+#             */
/*   Updated: 2019/05/03 18:21:49 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include "mlx.h"
# include "libft.h"
# include <fcntl.h>
# include <math.h>
# include "get_next_line.h"

typedef struct			s_list_fdf
{
	double				x0;
	double				y0;
	double				height0;
	double				x;
	double				y;
	double				height;
	struct s_list_fdf	*next;
}						t_point;

typedef struct			s_map
{
	void				*mlx_ptr;
	void				*mlx_window;
	size_t				max_x;
	size_t				len_array;
	double				angle_rotate;
	t_point				**first;
	double				zoom;
	double				start_x;
	double				start_y;
	long int			color;
}						t_map;

void					ft_output_in_display(t_point **first, t_map *map);
int						ft_split_and_list(char **line, size_t *max_x, int y
	, t_point **first);
int						ft_validation(char **argv);
void					ft_bresenham(t_point first, t_point second
	, t_map **map);
void					ft_list_in_iso(t_point **first, t_map *map);
void					ft_default(t_point **first, t_map *map);
void					ft_rotate_y(double *x, double *y, double *z
	, double angle_rotate);
void					ft_rotate_x(double *x, double *y, double *z
	, double angle_rotate);
void					ft_rotate_list(t_point **first, t_map *map
	, void (*f)(double*, double*, double*, double), double angle);
void					ft_exit(t_map **map);
void					ft_zoom(t_map *map, char znak);
void					ft_move(t_map *map, double znak, char per);
void					ft_change_color(t_map *map);
void					ft_print_usage(t_map *map);
t_point					*ft_score_file(char **argv, size_t *max_x);
void					ft_free_list(t_point **a);
t_point					*ft_in_array(t_point **start, size_t *len_array);

#endif
