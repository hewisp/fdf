/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/28 18:50:15 by hewisp            #+#    #+#             */
/*   Updated: 2019/04/12 21:11:44 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static char	*ft_strjoin_del(char *s1, char *s2)
{
	char *pointer;

	pointer = s1;
	s1 = ft_strjoin(s1, s2);
	free(pointer);
	free(s2);
	return (s1);
}

static int	ft_end(char **line, char *jopa, char **buffer, const int fd)
{
	char	*pointer;

	if (!(*line = ft_strjoin_del(*line, jopa)))
	{
		free(buffer[fd]);
		return (-1);
	}
	pointer = buffer[fd];
	if (!(buffer[fd] = ft_strdup(ft_strchr(buffer[fd], '\n') + 1)))
	{
		free(pointer);
		return (-1);
	}
	free(pointer);
	return (1);
}

static int	ft_check(char **buffer, const int fd, size_t *counter, char *jopa)
{
	int		a;

	if (!jopa)
		return (-1);
	if (!buffer[fd] || buffer[fd][0] == '\0' || buffer[fd][*counter] == '\0')
	{
		if (buffer[fd])
			free(buffer[fd]);
		if (!(buffer[fd] = ft_strnew(BUFF_SIZE)))
		{
			free(jopa);
			return (-1);
		}
		a = read(fd, buffer[fd], BUFF_SIZE);
		if (a <= 0)
		{
			free(jopa);
			return ((a == 0) ? (0) : (-1));
		}
	}
	return (1);
}

int			get_next_line(const int fd, char **ln)
{
	size_t			counter;
	char			*jopa;
	static	char	*buffer[65536];
	int				n;

	counter = 0;
	if (!ln || fd < 0 || fd >= 65536 || BUFF_SIZE <= 0 || !(*ln = ft_strnew(0)))
		return (-1);
	if ((n = ft_check(buffer, fd, &counter,
		jopa = ft_strnew(BUFF_SIZE))) <= 0)
		return (n);
	while (buffer[fd][counter] != '\n')
	{
		jopa[counter] = buffer[fd][counter];
		if (buffer[fd][++counter] == '\0')
		{
			if (!(*ln = ft_strjoin_del(*ln, jopa)))
				return (-1);
			if ((n = ft_check(buffer, fd, &counter,
				jopa = ft_strnew(BUFF_SIZE))) <= 0)
				return ((n == 0) ? (1) : (-1));
			counter = 0;
		}
	}
	return (ft_end(ln, jopa, buffer, fd));
}
