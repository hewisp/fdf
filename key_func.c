/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_func.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/03 18:05:12 by hewisp            #+#    #+#             */
/*   Updated: 2019/05/03 18:05:26 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	iso(t_point *first)
{
	first->x = (first->x0 - first->y0) * cos(0.523599);
	first->y = -(first->height0) + (first->x0 + first->y0) * sin(0.523599);
}

void	ft_rotate_x(double *x, double *y, double *z, double angle_rotate)
{
	double x1;

	x1 = *x;
	*x = ((*x) * cos(angle_rotate)) + ((*z) * sin(angle_rotate));
	*y = (*y);
	*z = (-(x1) * sin(angle_rotate)) + ((*z) * cos(angle_rotate));
}

void	ft_rotate_y(double *x, double *y, double *z, double angle_rotate)
{
	double y1;

	y1 = *y;
	*x = *x;
	*y = ((*y) * cos(angle_rotate)) + ((*z) * sin(angle_rotate));
	*z = (-y1 * sin(angle_rotate)) + ((*z) * cos(angle_rotate));
}

void	ft_default(t_point **first, t_map *map)
{
	size_t i;

	i = 0;
	mlx_clear_window(map->mlx_ptr, map->mlx_window);
	map->zoom = 1;
	map->start_x = 1000;
	map->start_y = 500;
	map->color = 16777215;
	while (i <= (map->len_array))
	{
		(*first)[i].x = (*first)[i].x0;
		(*first)[i].y = (*first)[i].y0;
		(*first)[i].height = (*first)[i].height0;
		i++;
	}
	ft_output_in_display((map->first), map);
}

void	ft_list_in_iso(t_point **first, t_map *map)
{
	size_t i;

	i = 0;
	mlx_clear_window(map->mlx_ptr, map->mlx_window);
	while (i <= (map->len_array))
	{
		iso(&(*first)[i]);
		i++;
	}
	ft_output_in_display((map->first), map);
}
