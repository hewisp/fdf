/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_func2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/03 18:08:11 by hewisp            #+#    #+#             */
/*   Updated: 2019/05/03 18:17:10 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	ft_rotate_list(t_point **first, t_map *map
	, void (*f)(double*, double*, double*, double), double angle)
{
	size_t i;

	i = 0;
	mlx_clear_window(map->mlx_ptr, map->mlx_window);
	while (i <= (map->len_array))
	{
		f(&(*first)[i].x, &(*first)[i].y, &(*first)[i].height, angle);
		i++;
	}
	ft_output_in_display((map->first), map);
}

void	ft_exit(t_map **map)
{
	free(*((*map)->first));
	exit(0);
}

void	ft_zoom(t_map *map, char znak)
{
	mlx_clear_window(map->mlx_ptr, map->mlx_window);
	if (znak == '+')
		map->zoom *= 1.1;
	else if (znak == '-')
		map->zoom *= 0.9;
	ft_output_in_display((map->first), map);
}

void	ft_move(t_map *map, double znak, char per)
{
	mlx_clear_window(map->mlx_ptr, map->mlx_window);
	if (per == 'x')
		map->start_x += znak;
	else if (per == 'y')
		map->start_y += znak;
	ft_output_in_display((map->first), map);
}

void	ft_change_color(t_map *map)
{
	mlx_clear_window(map->mlx_ptr, map->mlx_window);
	map->color -= 10;
	ft_output_in_display((map->first), map);
}
