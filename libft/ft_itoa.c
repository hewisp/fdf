/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/09 19:58:41 by hewisp            #+#    #+#             */
/*   Updated: 2018/12/12 19:04:15 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char	*ft_in_array(char *p, int n, size_t i)
{
	int del;
	int a;

	del = 10;
	a = n;
	if (n < 0)
		n = n * -1;
	while (i != 0)
	{
		p[i] = n % del + '0';
		n = n / 10;
		i--;
	}
	if (a > 0)
		p[i] = n + '0';
	else
		p[i] = '-';
	return (p);
}

char		*ft_itoa(int n)
{
	size_t	del;
	int		a;
	char	*p;

	del = 0;
	a = n;
	p = NULL;
	if (n == 0)
		return (ft_strdup("0"));
	if (n == -2147483648)
		return (ft_strdup("-2147483648"));
	while (a != 0)
	{
		a = a / 10;
		del++;
	}
	if (n < 0)
		p = ft_strnew(del);
	else
		p = ft_strnew(del--);
	if (p == NULL)
		return (NULL);
	p = ft_in_array(p, n, del);
	return (p);
}
