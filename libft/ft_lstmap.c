/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/09 15:03:00 by hewisp            #+#    #+#             */
/*   Updated: 2018/12/15 16:55:03 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list *list;
	t_list *new_list;

	if (lst && f)
	{
		list = f(lst);
		if (!list)
			return (NULL);
		new_list = list;
		while (lst->next)
		{
			lst = lst->next;
			list->next = f(lst);
			if (!(list->next))
			{
				ft_t_list_dell(&new_list);
				return (NULL);
			}
			list = list->next;
		}
		return (new_list);
	}
	return (NULL);
}
