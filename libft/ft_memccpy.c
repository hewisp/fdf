/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/28 15:55:03 by hewisp            #+#    #+#             */
/*   Updated: 2018/12/13 19:05:07 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dest, const void *source, int ch, size_t count)
{
	size_t					i;
	unsigned char			*p;
	unsigned const char		*p2;
	unsigned char			c;

	i = 0;
	p = (unsigned char*)dest;
	p2 = (unsigned const char*)source;
	c = (unsigned char)ch;
	while (i < count)
	{
		p[i] = p2[i];
		if (p2[i] == c)
		{
			i++;
			return (dest + i);
		}
		else
			i++;
	}
	return (NULL);
}
