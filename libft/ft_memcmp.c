/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/30 17:14:23 by hewisp            #+#    #+#             */
/*   Updated: 2018/12/13 19:03:58 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_memcmp(const void *dest, const void *source, size_t num)
{
	size_t				i;
	unsigned const char *a;
	unsigned const char *b;

	i = 0;
	a = (unsigned const char*)dest;
	b = (unsigned const char*)source;
	while (i < num)
	{
		if (a[i] != b[i])
			return (a[i] - b[i]);
		i++;
	}
	return (0);
}
