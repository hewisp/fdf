/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/07 18:11:45 by hewisp            #+#    #+#             */
/*   Updated: 2018/12/13 19:09:02 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putnbr_fd(int n, int fd)
{
	long long int	del;
	long long int	a;

	del = 10;
	a = (long long int)n;
	if (n == -2147483648)
		ft_putstr_fd("-2147483648", fd);
	else
	{
		if (n < 0)
		{
			ft_putchar_fd('-', fd);
			n = -n;
		}
		while (n / del > 0)
			del = del * 10;
		while (del > 1)
		{
			a = ((n % del) / (del / 10)) + '0';
			ft_putchar_fd(a, fd);
			del = del / 10;
		}
	}
}
