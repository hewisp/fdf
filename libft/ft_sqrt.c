/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sqrt.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 16:54:45 by hewisp            #+#    #+#             */
/*   Updated: 2018/12/13 19:09:28 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_sqrt(int nb)
{
	long int	max;
	long int	min;
	long int	mid;
	long int	x;

	max = (long int)nb;
	min = 0;
	while (min <= max && max - min != 1)
	{
		mid = ((unsigned int)(min + max) / 2);
		x = mid * mid;
		if (x == nb)
			return (mid);
		if (nb < x)
			max = mid;
		else
			min = mid;
	}
	return (0);
}
