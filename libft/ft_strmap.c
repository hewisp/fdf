/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/05 15:54:39 by hewisp            #+#    #+#             */
/*   Updated: 2018/12/13 18:44:45 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	size_t	i;
	size_t	s_len;
	char	*p;

	i = 0;
	p = NULL;
	if (s && f)
	{
		s_len = ft_strlen(s);
		p = ft_strnew(s_len);
		if (p == NULL)
			return (NULL);
		while (s[i])
		{
			p[i] = f(s[i]);
			i++;
		}
	}
	return (p);
}
