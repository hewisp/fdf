/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/05 16:22:56 by hewisp            #+#    #+#             */
/*   Updated: 2018/12/13 18:45:34 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	size_t	i;
	size_t	s_len;
	char	*p;

	i = 0;
	p = NULL;
	if (s && f)
	{
		s_len = ft_strlen(s);
		p = ft_strnew(s_len);
		if (p == NULL)
			return (NULL);
		while (s[i])
		{
			p[i] = f(i, s[i]);
			i++;
		}
	}
	return (p);
}
