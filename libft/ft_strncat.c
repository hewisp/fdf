/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/27 20:56:31 by hewisp            #+#    #+#             */
/*   Updated: 2018/12/15 18:37:35 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncat(char *destptr, const char *srcptr, size_t num)
{
	size_t i;
	size_t i2;

	i = 0;
	i2 = 0;
	while (destptr[i])
	{
		i++;
	}
	while (i2 < num && srcptr[i2])
	{
		destptr[i] = srcptr[i2];
		i++;
		i2++;
	}
	destptr[i] = '\0';
	return (destptr);
}
