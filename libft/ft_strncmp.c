/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/29 20:25:03 by hewisp            #+#    #+#             */
/*   Updated: 2018/12/11 16:45:29 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_strncmp(const char *str1, const char *str2, size_t n)
{
	size_t index;

	if (n == 0)
		return (0);
	index = 0;
	while ((((unsigned char)str1[index] != '\0' ||
	(unsigned char)str2[index] != '\0')) && index < n)
	{
		if ((unsigned char)str1[index] != (unsigned char)str2[index])
			return ((unsigned char)str1[index] - (unsigned char)str2[index]);
		index++;
	}
	return (0);
}
