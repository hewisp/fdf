/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/26 18:51:01 by hewisp            #+#    #+#             */
/*   Updated: 2018/12/09 20:01:32 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncpy(char *destination, const char *source, size_t n)
{
	size_t index;

	index = 0;
	if (!source[index])
		destination[index] = source[index];
	while (index < n && source[index])
	{
		destination[index] = source[index];
		index++;
	}
	while (index < n)
	{
		destination[index] = '\0';
		index++;
	}
	return (destination);
}
