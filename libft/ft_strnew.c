/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/04 19:06:59 by hewisp            #+#    #+#             */
/*   Updated: 2018/12/13 16:02:46 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnew(size_t size)
{
	char	*p;
	size_t	a;

	a = 0;
	if (size == a - 1)
		return (NULL);
	p = (char*)malloc(sizeof(char) * (size + 1));
	if (p == NULL)
		return (NULL);
	ft_bzero(p, size + 1);
	return (p);
}
