/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_t_list_dell.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/15 15:35:20 by hewisp            #+#    #+#             */
/*   Updated: 2018/12/15 15:53:45 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_t_list_dell(t_list **alst)
{
	t_list *p;
	t_list *next_p;

	p = (*alst);
	if (*alst)
	{
		while (p)
		{
			next_p = p->next;
			free(p->content);
			free(p);
			p = next_p;
		}
		(*alst) = NULL;
	}
}
