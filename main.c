/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 14:59:17 by hewisp            #+#    #+#             */
/*   Updated: 2019/05/03 18:43:31 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	ft_output_in_display(t_point **first, t_map *map)
{
	size_t i;

	i = 0;
	while (((*first)[i]).next)
	{
		if ((i + 1) % map->max_x != 0)
			ft_bresenham((*first)[i], (*first)[i + 1], &map);
		if (i + map->max_x <= map->len_array)
		{
			ft_bresenham((*first)[i], (*first)[i + map->max_x], &map);
		}
		i++;
	}
	ft_print_usage(map);
}

void	ft_key2(int key, t_map *map)
{
	if (key == 13)
		ft_move(map, -10, 'y');
	else if (key == 1)
		ft_move(map, 10, 'y');
	else if (key == 8)
		ft_change_color(map);
	else if (key == 34)
		ft_list_in_iso(map->first, map);
	else if (key == 40)
		ft_default(map->first, map);
}

int		ft_key(int key, t_map *map)
{
	if (key == 53)
		ft_exit(&map);
	else if (key == 124)
		ft_rotate_list((map->first), map, &ft_rotate_x, map->angle_rotate);
	else if (key == 123)
		ft_rotate_list((map->first), map, &ft_rotate_x, -(map->angle_rotate));
	else if (key == 126)
		ft_rotate_list((map->first), map, &ft_rotate_y, (map->angle_rotate));
	else if (key == 125)
		ft_rotate_list((map->first), map, &ft_rotate_y, -(map->angle_rotate));
	else if (key == 24)
		ft_zoom(map, '+');
	else if (key == 27)
		ft_zoom(map, '-');
	else if (key == 2)
		ft_move(map, 10, 'x');
	else if (key == 0)
		ft_move(map, -10, 'x');
	else
		ft_key2(key, map);
	return (0);
}

int		ft_open_window(t_point **first, size_t max_x, size_t len_array)
{
	t_map	map;
	size_t	i;

	i = 0;
	if (!(map.mlx_ptr = mlx_init()))
		return (-1);
	map.mlx_window = mlx_new_window(map.mlx_ptr, 2000, 1300, "fdf");
	map.max_x = max_x + 1;
	map.len_array = len_array;
	map.first = first;
	map.angle_rotate = 0.1;
	map.zoom = 1;
	map.start_x = 1000;
	map.start_y = 500;
	map.color = 16777215;
	ft_output_in_display((map.first), &map);
	mlx_key_hook(map.mlx_window, ft_key, (void*)&map);
	mlx_loop(map.mlx_ptr);
	return (1);
}

int		main(int argc, char **argv)
{
	t_point *first;
	t_point *array;
	size_t	max_x;
	size_t	len_array;

	first = NULL;
	max_x = 0;
	if (argc == 2)
	{
		if (ft_validation(argv))
		{
			if (!(first = ft_score_file(argv, &max_x)))
			{
				write(1, "Validation error\n", 17);
				return (0);
			}
			array = ft_in_array(&first, &len_array);
			ft_free_list(&first);
			ft_open_window(&array, max_x, len_array);
		}
		else
			ft_putendl("Validation error");
	}
	return (0);
}
