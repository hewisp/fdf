/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mix.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/03 18:12:57 by hewisp            #+#    #+#             */
/*   Updated: 2019/05/03 18:17:14 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_point	*ft_in_array(t_point **start, size_t *len_array)
{
	t_point *list;
	size_t	i;
	t_point	*array;

	*len_array = 0;
	list = *start;
	i = 0;
	while (list && list->next)
	{
		(*len_array)++;
		list = list->next;
	}
	list = *start;
	if (!(array = (t_point*)malloc(sizeof(t_point) * ((*len_array) + 1))))
		return (NULL);
	while (i <= (*len_array))
	{
		array[i] = *list;
		i++;
		list = list->next;
	}
	list = *start;
	return (array);
}

void	ft_free_list(t_point **a)
{
	t_point *list;
	t_point *next;

	list = *a;
	while (list)
	{
		next = list->next;
		free(list);
		list = next;
	}
}

t_point	*ft_score_file(char **argv, size_t *max_x)
{
	char	*line;
	int		fd;
	int		y;
	t_point	*first;

	first = NULL;
	y = 0;
	if (!(fd = open(argv[1], O_RDONLY)))
		return (NULL);
	while (get_next_line(fd, &line) > 0)
	{
		if (ft_split_and_list(&line, max_x, y, &first) == -1)
		{
			if (first)
				ft_free_list(&first);
			if (line)
				free(line);
			return (NULL);
		}
		if (line)
			free(line);
		y++;
	}
	return (first);
}

void	ft_print_usage2(t_map *map)
{
	mlx_string_put(map->mlx_ptr, map->mlx_window, 10, 165, 0xFFFFFF
		, "a     - move left           |");
	mlx_string_put(map->mlx_ptr, map->mlx_window, 10, 185, 0xFFFFFF
		, "+,-   - zoom                |");
	mlx_string_put(map->mlx_ptr, map->mlx_window, 10, 205, 0xFFFFFF
		, "c     - change color        |");
	mlx_string_put(map->mlx_ptr, map->mlx_window, 10, 225, 0xFFFFFF
		, "i     - iso                 |");
	mlx_string_put(map->mlx_ptr, map->mlx_window, 10, 245, 0xFFFFFF
		, "k     - default             |");
	mlx_string_put(map->mlx_ptr, map->mlx_window, 10, 265, 0xFFFFFF
		, "esq   - exit                |");
	mlx_string_put(map->mlx_ptr, map->mlx_window, 10, 285, 0xFFFFFF
		, "____________________________|");
}

void	ft_print_usage(t_map *map)
{
	mlx_string_put(map->mlx_ptr, map->mlx_window, 10, 5, 0xFFFFFF
		, "           USAGE            |");
	mlx_string_put(map->mlx_ptr, map->mlx_window, 10, 25, 0xFFFFFF
		, "left  - rotate figure left  |");
	mlx_string_put(map->mlx_ptr, map->mlx_window, 10, 45, 0xFFFFFF
		, "right - rotate figure right |");
	mlx_string_put(map->mlx_ptr, map->mlx_window, 10, 65, 0xFFFFFF
		, "up    - rotate figure up    |");
	mlx_string_put(map->mlx_ptr, map->mlx_window, 10, 85, 0xFFFFFF
		, "down  - rotate figure down  |");
	mlx_string_put(map->mlx_ptr, map->mlx_window, 10, 105, 0xFFFFFF
		, "w     - move up             |");
	mlx_string_put(map->mlx_ptr, map->mlx_window, 10, 125, 0xFFFFFF
		, "s     - move down           |");
	mlx_string_put(map->mlx_ptr, map->mlx_window, 10, 145, 0xFFFFFF
		, "d     - move right          |");
	ft_print_usage2(map);
}
