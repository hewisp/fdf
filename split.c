/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   split.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/03 17:13:11 by hewisp            #+#    #+#             */
/*   Updated: 2019/05/03 17:14:24 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		ft_validation(char **argv)
{
	int		fd;
	char	buf;

	fd = 0;
	if (!(fd = open(argv[1], O_RDONLY)))
	{
		perror(argv[1]);
		return (-1);
	}
	while (read(fd, &buf, 1) > 0)
	{
		if (buf != '\n' && buf != ' ' && buf != '-' && buf != ',' && buf != 'x'
		&& !(buf >= '0' && buf <= '9')
		&& !(buf >= 'A' && buf <= 'Z'))
		{
			close(fd);
			return (0);
		}
	}
	close(fd);
	return (1);
}

void	ft_del_dv_array(char ***array)
{
	size_t i;

	i = 0;
	while ((*array)[i])
	{
		free((*array)[i]);
		i++;
	}
}

int		ft_split_and_list_2(t_point **list, t_point **first, int *i
	, char **split_array)
{
	if (!(*list))
	{
		if (!((*list) = (t_point*)malloc(sizeof(t_point))))
			return (-1);
		(*list)->x = i[0] * 16;
		(*list)->y = i[1] * 16;
		(*list)->height = ft_atoi(split_array[i[0]]) * 16;
		(*list)->x0 = i[0] * 16;
		(*list)->y0 = i[1] * 16;
		(*list)->height0 = ft_atoi(split_array[i[0]]) * 16;
		*first = (*list);
	}
	else
	{
		if (!((*list)->next = (t_point*)malloc(sizeof(t_point))))
			return (-1);
		(*list)->next->x = i[0] * 16;
		(*list)->next->y = i[1] * 16;
		(*list)->next->height = ft_atoi(split_array[i[0]]) * 16;
		(*list)->next->x0 = i[0] * 16;
		(*list)->next->y0 = i[1] * 16;
		(*list)->next->height0 = ft_atoi(split_array[i[0]]) * 16;
		(*list) = (*list)->next;
	}
	return (0);
}

int		ft_exit_split(int *i, size_t *max_x)
{
	if (i[1] == 0)
		(*max_x) = i[0] - 1;
	else if (i[0] != (int)(*max_x) + 1)
		return (-1);
	return (0);
}

int		ft_split_and_list(char **line, size_t *max_x, int y, t_point **first)
{
	char	**split_array;
	int		i[2];
	t_point	*list;

	i[0] = 0;
	i[1] = y;
	if (*first)
		list = (*first);
	else
		list = NULL;
	if (!(split_array = ft_strsplit(*line, ' ')) || !(*split_array))
		return (-1);
	while (list && (list->next))
		list = list->next;
	while (split_array[i[0]])
	{
		if ((ft_split_and_list_2(&list, first, i, split_array)) == -1)
			return (-1);
		i[0]++;
	}
	ft_del_dv_array(&split_array);
	list->next = NULL;
	return (ft_exit_split(i, max_x));
}
